# Projet Rover Martien

Ce projet vise à résoudre le problème de Rover martien.

### :construction_worker: Technologies utilisées

* [Typescript](https://www.typescriptlang.org/)
* [NodeJs](https://nodejs.org/)
  
### :rocket: Installation

1. Cloner le dépôt Gitlab

2. Installer node version > 18

3. Installer les dépendences
```
cd rover-martien
npm install
```

4. Lancer le projet
```
npm run start
```