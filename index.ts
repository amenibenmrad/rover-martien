import { OrientationEnum } from "./models/orientation.enum";
import { Robot } from "./models/robot.class";

function robotPerdu(robot: Robot) {
    robot.perdu = true;
}

function commanderRobots(largeurGrille: number, hauteurGrille: number, robotsEtCommandes: [number, number, string, string][]) {
    robotsEtCommandes.forEach(([x, y, orientation, commandes], index) => {
        const robot = new Robot(x, y, orientation as OrientationEnum);
        for (const char of commandes) {
            if(!robot.perdu) {
                switch (char) {
                    case 'F':
                        avancerRobot(robot, largeurGrille, hauteurGrille);
                        break;
                    case 'L':
                        robot.tournerAGauche();
                        break;
                    case 'R':
                        robot.tournerADroite();
                        break;
                    default:
                        throw new Error(`Merci de saisir une commande valide parmi (F, L, R)`);
                }
            }
        }
        console.log(`----Robot n°: ${index + 1}----`)
        robot.afficherInformations();
    })
}

function avancerRobot(robot: Robot, largeurGrille: number, hauteurGrille: number) {
    switch (robot.orientation) {
        case OrientationEnum.NORD:
            if (robot.y + 1 > hauteurGrille) {
                robotPerdu(robot);
                break;
            }
            robot.avancerY(1)
            break;

        case OrientationEnum.EST:
            if (robot.x + 1 > largeurGrille) {
                robotPerdu(robot);
                break;
            }
            robot.avancerX(1)
            break;

        case OrientationEnum.OUEST:
            if (robot.x - 1 < 0) {
                robotPerdu(robot);
                break;
            }
            robot.avancerX(-1)
            break;

        case OrientationEnum.SUD:
            if (robot.y - 1 < 0) {
                robotPerdu(robot);
                break;
            }
            robot.avancerY(-1)
            break;

        default:
            throw new Error(`Merci de saisir une orientation valide parmi (${Robot.orientations.join()})`);
    }
}

// Un cas de test
commanderRobots(4, 8,
    [
        [2, 3, 'N', 'FLLFR'],
        [1, 0, 'S', 'FFRLF'],
    ]
)
