export enum OrientationEnum {
    EST = 'E',
    NORD = 'N',
    OUEST = 'W',
    SUD = 'S',
}