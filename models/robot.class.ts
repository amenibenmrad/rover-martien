import { OrientationEnum } from "./orientation.enum";

export class Robot {
    static orientations: OrientationEnum[] =
        [OrientationEnum.NORD, OrientationEnum.EST, OrientationEnum.SUD, OrientationEnum.OUEST]
    x: number;
    y: number;
    orientation: OrientationEnum;
    perdu: boolean;

    constructor(x: number, y: number, orientation: OrientationEnum) {
        this.x = x;
        this.y = y;
        this.orientation = orientation
        this.perdu = false;
    }

    avancerX(nombrePas: number) {
        this.x += nombrePas;
    }

    avancerY(nombrePas: number) {
        this.y += nombrePas;
    }

    tournerADroite() {
        let indexDirection = Robot.orientations.indexOf(this.orientation);
        this.orientation = Robot.orientations[(indexDirection + 1) % 4];
    }

    tournerAGauche() {
        let indexDirection = Robot.orientations.indexOf(this.orientation);
        this.orientation = Robot.orientations[(indexDirection + 3) % 4];
    }

    afficherInformations() {
        console.log(`${this.x} ${this.y} ${this.orientation} ${this.perdu ? 'Lost': ''}\n`)
    }

}
